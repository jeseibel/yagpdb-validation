# YAGPDB-validation

## What is this?

This is a script for Yet Another General Purpose Discord Bot (YAGPDB or YAG) that listens for a secret phrase before assigning a role.

This was originally made for my own Discord server to make sure people read the FAQ before posting questions that were answered in said FAQ (and to reduce the number of spam bots).


## How do I use it?

1. In your server's control panel on YAG's website go to: Core -> Custom Commands
2. Create a new custom command
3. Change the trigger type to regex and make the trigger ".\*" (so it will trigger with every message)
4. Set the Channel restrictions so it will "Only run in the following channels", and select your validation channel
5. Paste the contents of discord_validation_bot_code.cs into the response
6. Save
7. (optional) In your discord create a channel "bot-log" which YAG can send messages to.
	Whenever a incorrect validation is given, YAG will log the message here.
