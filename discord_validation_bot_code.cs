{{ $failMessageDisplayTimeInSeconds := 40 }}
{{ $successMessageDisplayTimeInSeconds := 30 }}


{{/* delete the message */}}
{{ deleteMessage nil .Message.ID 0 }}

{{/* TODO: it might be worthwhile to make this case-insensitive, but that can be done another time */}}
{{/* formerly: "[Tt]riangles are the other white meat" */}}
{{$regexWordMatches := reFindAll "(?i)ask questions in the help me channel" .Message.Content}}
{{ if eq (len $regexWordMatches) 1 }}

	{{/* add the "Read The FAQ" role */}}
	{{ addRoleID 999494011213586533 }}
	
	{{/* send the success message */}}
	{{$embed := cembed 
		"title" "Validation Successful" 
		"description" (print "<@" .User.ID ">\n You are now able to send messages. \n\nIf you need help or have questions, no matter how small,\n please post in the <#1030105983546052618> channel.")
		"color" 9287168 
	}}
	{{ $tempMessageId := sendMessageRetID nil $embed }}
	{{ sendDM $embed }}
	{{ deleteMessage nil $tempMessageId $successMessageDisplayTimeInSeconds }}
	
	
	{{/* Log the successful validation */}}
	{{$embed := cembed 
		"title" "Validation Success" 
		"description" .Message.Content
		"color" 9287168 
		"fields" (cslice 
			(sdict "name" "User" "value" (.User.Mention) "inline" true) 
		)
		"thumbnail" (sdict "url" (joinStr "" "https://cdn.discordapp.com/avatars/" (toString .User.ID) "/" .User.Avatar ".png")) 
		"timestamp" .Message.Timestamp
	}}
	{{ sendMessage "validation-log" $embed }}
	
{{ else }}
	
	{{/* send the fail message */}}
	{{$embed := cembed 
		"title" "Validation Fail" 
		"description" (print "<@" .User.ID ">\n Please read the FAQ for a secret code to prove you have read the FAQ, afterwards send it here.")
		"fields" (cslice 
			(sdict "name" "Hint" "value" "**You don't have to read every word in the FAQ**, __just reading what questions are answered__ will lead you to the code." "inline" false) 
			(sdict "name" "Additional Help" "value" "If you found the secret code, sent it in <#1006055322915438672>, and the validation still isn't working; please Direct Message James or a moderator." "inline" false) 
		)
		"color" 16711680 
	}}
	{{ $tempMessageId := sendMessageRetID nil $embed }}
	{{ deleteMessage nil $tempMessageId $failMessageDisplayTimeInSeconds }}
	
	{{/* send the fail DM */}}
	{{$embed := cembed 
		"title" "Validation Fail" 
		"description" (print "<@" .User.ID ">\n Please read the FAQ for a secret code to prove you have read the FAQ, afterwards send it in <#1006055322915438672>.")
		"fields" (cslice 
			(sdict "name" "Hint" "value" "**You don't have to read every word in the FAQ**, __just reading what questions are answered__ will lead you to the code." "inline" false) 
			(sdict "name" "Additional Help" "value" "If you found the secret code, sent it in <#1006055322915438672>, and the validation still isn't working; please Direct Message James or a moderator." "inline" false) 
		)
		"color" 16711680 
	}}
	{{ sendDM $embed }}

	
	{{/* Log the deleted message */}}
	{{$embed := cembed 
		"title" "Incorrect Validation Message" 
		"description" .Message.Content
		"color" 16711680 
		"fields" (cslice 
			(sdict "name" "User" "value" (.User.Mention) "inline" true) 
		)
		"thumbnail" (sdict "url" (joinStr "" "https://cdn.discordapp.com/avatars/" (toString .User.ID) "/" .User.Avatar ".png")) 
		"timestamp" .Message.Timestamp
	}}
	{{ sendMessage "validation-log" $embed }}
	
{{ end }}

